# 9. Présentation des conteneurs dinitialisation

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

### Objectif

Nous allons parler des conteneurs d'initialisation. Voici un aperçu rapide de ce que nous allons aborder. Nous allons expliquer ce que sont les conteneurs d'initialisation, parler de quelques cas d'utilisation des conteneurs d'initialisation, puis nous ferons une brève démonstration pratique.

#### Qu'est-ce qu'un Conteneur d'initialisation ?

les conteneurs d'initialisation sont essentiellement des conteneurs qui s'exécutent une fois jusqu'à leur achèvement pendant le processus de démarrage d'un pod. Il ne s'agit pas des conteneurs d'application principaux qui s'exécutent en continu. Ils ne s'exécutent qu'une fois au démarrage du pod. Un pod peut avoir plusieurs conteneurs init, et chacun de ces conteneurs init s'exécute une fois jusqu'à leur achèvement.

#### Processus de Démarrage d'un Pod

Voyons une chronologie rapide du processus de démarrage d'un pod. Disons que nous avons plusieurs conteneurs init dans ce pod. La première chose qui se passe est que le premier conteneur init s'exécute jusqu'à son achèvement avant que quoi que ce soit d'autre ne se produise. Si nous avons un deuxième conteneur init, il s'exécute également jusqu'à son achèvement après que le premier conteneur init soit terminé. Une fois que tous les conteneurs d'initialisation ont terminé, les conteneurs d'application peuvent démarrer. Chaque conteneur init s'exécute dans l'ordre, et une fois qu'ils sont tous terminés, les conteneurs d'application peuvent s'exécuter.

#### Cas d'Utilisation des conteneurs d'initialisation

les conteneurs d'initialisation peuvent être utilisés pour effectuer diverses tâches de démarrage. Ils peuvent contenir et utiliser des logiciels et des scripts de configuration qui ne sont pas nécessaires aux conteneurs principaux, permettant ainsi de garder vos conteneurs principaux plus légers en déchargeant certaines tâches de démarrage initiales à ces conteneurs init.

1. **Attente de la Création d'une Ressource Kubernetes** : Par exemple, vous pouvez utiliser un conteneur init pour suspendre le démarrage jusqu'à ce qu'un service ou un autre pod soit disponible.

2. **Exécution Sécurisée de Tâches Sensibles** : Si vous avez des étapes à exécuter uniquement pendant le démarrage d'un pod et que ces étapes nécessitent de travailler avec des données sensibles comme un mot de passe, vous pouvez les confier à un conteneur init pour garantir que ces données sensibles ne soient pas compromises si le conteneur principal est compromis.

3. **Données dans un Volume Partagé** : Si vous devez écrire des données dans un volume que votre conteneur principal lira au démarrage, vous pouvez utiliser un conteneur init pour cela.

4. **Communication avec un Service Externe** : Par exemple, si vous devez enregistrer votre pod auprès d'un service externe, vous pouvez utiliser un conteneur init pour effectuer cette tâche, car elle ne doit se produire qu'au démarrage.

#### Démonstration Pratique

Voyons à quoi ressemblent les conteneurs d'initialisation dans un cluster Kubernetes réel. Pour commencer, je vais créer ce fichier de définition de pod appelé `init-pod.yml` et créer un pod nginx très simple avec un seul conteneur nginx. Pour créer un conteneur init, il suffit d'ajouter cette liste de conteneurs init sous la spécification du pod.

Voici un exemple de manifeste YAML pour un pod avec un conteneur init :

```yaml
apiVersion: v1
kind: Pod
metadata:
  name: init-pod
spec:
  initContainers:
  - name: delay
    image: busybox
    command: ["sh", "-c", "sleep 30"]
  containers:
  - name: nginx
    image: nginx
```

Dans cet exemple, nous avons notre conteneur d'application principal et un conteneur init appelé `delay`, qui utilise l'image busybox et exécute la commande `sleep 30`. Ce conteneur init va simplement retarder le démarrage de notre application principale de 30 secondes.

Pour créer le pod avec ce manifeste YAML, utilisez la commande suivante :

```sh
kubectl apply -f init-pod.yml
```

Ensuite, vérifiez le statut de notre pod :

```sh
kubectl get pod init-pod
```

Vous verrez que le statut est actuellement en phase d'initialisation (init). Bien sûr, notre conteneur d'application principal n'est pas encore prêt car il n'a pas encore commencé à démarrer. Vous verrez `0/1` sous la colonne `READY`, ce qui représente le nombre de conteneurs init terminés. Après 30 secondes, le conteneur init sera terminé et le conteneur d'application principal démarrera.

### Résumé de la Leçon
Dans cette leçon, nous avons parlé des conteneurs d'initialisation, couvert quelques exemples d'utilisation et effectué une démonstration pratique pour montrer comment utiliser les conteneurs d'initialisation dans Kubernetes. C'est tout pour cette leçon. À la prochaine !


# Reférences 



https://kubernetes.io/docs/concepts/workloads/pods/init-containers/